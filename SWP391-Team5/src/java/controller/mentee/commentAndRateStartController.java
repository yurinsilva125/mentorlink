/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.mentee;

import dao.FeedbackDAO;
import dao.MentorCVDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.CV_Mentor;
import model.Feedback;
import model.User;

/**
 *
 * @author admin
 */
@WebServlet(name = "commentAndRateStartController", urlPatterns = {"/commentAndRateStart"})
public class commentAndRateStartController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        int rating = Integer.parseInt(request.getParameter("rating"));
        String opinion = request.getParameter("opinion");
        HttpSession session = request.getSession();
        User abc = (User) session.getAttribute("acc");
        int user_id = Integer.parseInt(abc.getUser_id());
//      int user_id = 3;
        int mentor_id = Integer.parseInt(request.getParameter("mentor_id"));

        FeedbackDAO fb = new FeedbackDAO();
        if (fb.insertFeedback(user_id, mentor_id, rating, opinion)) {
            MentorCVDAO dao = new MentorCVDAO();
            CV_Mentor cv = dao.getCvMentorById(String.valueOf(mentor_id));
            List<CV_Mentor> list = dao.getAllListMentor();
            FeedbackDAO dao1 = new FeedbackDAO();
            List<Feedback> listF = dao1.getAllFeedbackOfMentor(mentor_id);
            request.setAttribute("mentor_id", mentor_id);
            request.setAttribute("cv", cv);
            request.setAttribute("listMentor", list);
            request.setAttribute("listF", listF);
            request.getRequestDispatcher("common/viewCvMentor.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
