<%-- 
    Document   : footer
    Created on : Jun 11, 2023, 1:25:17 PM
    Author     : Tuan Vinh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <footer id="footer" class="nomargin" style="border-top: none;">
        <div class="container mb-4">
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <div class="footer_heading footer_heading_contact">
                        <h4>Thông tin liên hệ</h4>
                    </div>
                    <div class="footer_contact">
                        <p>Điện thoại: <strong>036 3385579</strong></p>
                        <p>Email: contact@mentori.vn</p>

                        <p>CÔNG TY CỔ PHẦN GIẢI PHÁP KẾT NỐI MENTORI VIỆT NAM</p>
                        <p>Địa chỉ trụ sở chính: Satom Office - Trung Kính - Cầu Giấy - Hà Nội</p>

                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <div class="footer_col">
                                <div class="footer_heading">
                                    <h4>Thông tin</h4>
                                </div>
                                <div class="footer_list_item">
                                    <ul class="list-group">
                                        <li><a href="#" target="_blank" title="Về chúng tôi">Về
                                                chúng tôi</a></li>
                                        <li><a href="#" target="_blank">Cộng đồng</a></li>
                                        <li><a href="#" target="_blank">Trung tâm hỗ trợ</a></li>


                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="footer_col">
                                <div class="footer_heading">
                                    <h4>Khoá học</h4>
                                </div>
                                <div class="footer_list_item">
                                    <ul class="list-group">

                                        <li><a href="#" title="Khóa học của tôi">Khóa học của tôi</a></li>
                                        <li><a href="#" target="_blank" title="Dành cho bạn">Dành cho bạn</a>
                                        </li>
                                        <li><a href="#" target="_blank" title="Chính sách hoàn tiền">Chính sách hoàn
                                                tiền</a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="footer_col">
                                <div class="footer_heading">
                                    <h4>Chương trình cố vấn</h4>
                                </div>
                                <div class="footer_list_item">
                                    <ul class="list-group">
                                        <li><a href="#" title="Danh sách mentor">Danh sách mentor</a></li>
                                        <li><a href="#" title="Lĩnh vực">Lĩnh vực</a></li>
                                        <li><a href="#" title="Chủ đề">Chủ đề</a></li>
                                        <li><a href="#" target="_blank" title="Mentor Blog">Mentor Blog</a>
                                        </li>
                                        <li><a href="#" target="_blank" title="Đăng ký làm mentor">Đăng ký
                                                làm mentor</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="copyrights">
            <div class="container-md">
                <p>Bản quyền 2020 thuộc về Mentori</p>
            </div>
        </div>
    </footer>
    </body>
</html>
